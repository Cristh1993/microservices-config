FROM adoptopenjdk/openjdk8:latest
RUN mkdir /usr/src/configserver
COPY config-server-0.0.1-SNAPSHOT.jar /usr/src/configserver
WORKDIR /usr/src/configserver
EXPOSE 9090
CMD ["java", "-jar", "/usr/src/configserver/config-server-0.0.1-SNAPSHOT.jar"]